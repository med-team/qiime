# Startup script for ZSH with Qiime environment.
if [ -e "$ZOLDDOTDIR/.zshrc" ] ; then
    source "$ZOLDDOTDIR/.zshrc"
    unset ZOLDDOTDIR
fi

source /usr/lib/qiime/shell/qiime_environment

alias help='man qiime'

PROMPT="$PROMPT qiime > "
